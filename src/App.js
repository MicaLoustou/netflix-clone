import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import CategoryCardLists from './Components/CategoryCardLists';
import Header from './Components/Header';
import Hero from './Components/Hero';
import MoviePreview from './Components/MoviePreview';
import Footer from './Components/Footer';
import Splash from './Components/Splash';

function App() {
	return (
		<>
			<Router>
				<Routes>
					<Route
						exact
						path="/"
						element={
							<>
								<Splash />
								<Header />
								<Hero />
								<CategoryCardLists />
								<Footer />
							</>
						}
					></Route>
					<Route exact path="/movie" element={<MoviePreview />}></Route>
				</Routes>
			</Router>
		</>
	);
}

export default App;
