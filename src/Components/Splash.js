import React, { useState } from 'react';
import { Modal, Box, Backdrop, CircularProgress } from '@mui/material';
import { styled } from '@mui/system';
import YouTube from 'react-youtube';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		height: '100vh',
		width: '100vw',
		zIndex: '3000 !important',
	},
	box: {
		height: '100vh',
		width: '100vw',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		'& > div': {
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
			height: '120vh',
			width: '120vw',
		},
	},
});

// Auxiliary Components
const CustomBackdrop = () => {
	return (
		<Backdrop open={true} sx={{ zIndex: -1, backgroundColor: 'black' }}>
			<CircularProgress
				sx={{
					color: 'white',
					width: '64px !important',
					height: '64px !important',
				}}
			/>
		</Backdrop>
	);
};

// Render
const Splash = () => {
	const classes = useStyles();
	const [showSplash, setShowSplash] = useState(true);
	return (
		<Modal
			open={showSplash}
			disableEscapeKeyDown
			className={classes.root}
			BackdropComponent={styled(CustomBackdrop, {
				name: 'MuiModal',
				slot: 'Backdrop',
				overridesResolver: (props, styles) => {
					return styles.backdrop;
				},
			})({ zIndex: -1 })}
		>
			<Box className={classes.box}>
				<YouTube
					videoId="GV3HUDMQ-F8"
					opts={{
						height: '100%',
						width: '100%',
						title: 'Splash',
						playerVars: {
							autoplay: 1,
							controls: 0,
							mute: 1,
							disablekb: 1,
							fs: 0,
							iv_load_policy: 3,
							rel: 0,
							showinfo: 0,
						},
					}}
					onEnd={() => {
						setShowSplash(false);
					}}
				/>
			</Box>
		</Modal>
	);
};

export default Splash;
