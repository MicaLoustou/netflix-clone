import React, { useState, useEffect } from 'react';
import { SimilarMoviesApi } from '../helpers/MoviesApi';
import {
	Card,
	CardMedia,
	CardContent,
	Typography,
	IconButton,
	Grid,
} from '@mui/material';

import AddIcon from '@mui/icons-material/Add';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
	grid: {
		paddingLeft: '0 !important ',
		cursor: 'pointer',
	},
	similarCards: {
		color: '#fff !important',
		backgroundColor: '#2f2f2f !important',
		width: 224,
	},
	movieDetails: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-beetwen',
		alignItems: 'center',
	},
	minAge: {
		border: '1px solid #fff',
		padding: '0 16px',
		margin: '0 12px',
		height: 'max-content',
	},
	iconButtons: {
		backgroundColor: 'rgba(51,51,51,.6) !important',
		color: '#fff !important',
		marginRight: '12px !important',
		height: 'max-content',
	},
	overview: {
		height: 56,
		overflow: 'hidden',
	},
}));

const SimilarMovies = ({ movieType, movieData }) => {
	const classes = useStyles();
	const [similar, setSimilar] = useState([]);

	useEffect(() => {
		movieData &&
			movieType &&
			SimilarMoviesApi(movieType, movieData?.id, setSimilar);
	}, [movieData, movieType]);

	return (
		<>
			{similar?.results?.slice(0, 9).map((el, index) => {
				return (
					<Grid key={index} item className={classes.grid}>
						<Card className={classes.similarCards}>
							<CardMedia
								component="img"
								height="140"
								image={`https://image.tmdb.org/t/p/w500${el?.poster_path}`}
								alt="movie image"
							/>
							<CardContent>
								<div className={classes.movieDetails}>
									<div className={classes.movieDetails}>
										<p>
											{el?.release_date
												? new Date(el?.release_date).getFullYear()
												: ''}
										</p>
										<p className={classes.minAge}>12</p>
									</div>
									<IconButton
										className={classes.iconButtons}
										variant="outlined"
										aria-label="delete"
									>
										<AddIcon />
									</IconButton>
								</div>
								<Typography className={classes.overview} variant="body2">
									{el?.overview}
								</Typography>
							</CardContent>
						</Card>
					</Grid>
				);
			})}
		</>
	);
};

export default SimilarMovies;
