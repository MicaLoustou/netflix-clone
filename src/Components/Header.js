import React, { useState } from 'react';
import { AvatarIcon } from '../assets/AvatarIcon.js';
import { MoviesApi } from '../helpers/MoviesApi';
import SearchView from './SearchView';

// Components
import {
	AppBar,
	Box,
	IconButton,
	Typography,
	MenuItem,
	Menu,
	Avatar,
	Badge,
	Divider,
	TextField,
	InputAdornment,
} from '@mui/material';

//Icons
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsIcon from '@mui/icons-material/Notifications';
import CloseIcon from '@mui/icons-material/Close';

//Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(theme => ({
	root: {
		height: 80,
		background:
			'linear-gradient(180deg, rgba(20,20,20,1) 27%, rgba(0,0,0,0.5270483193277311) 100%) !important',
		padding: '0 4%',
		flexDirection: 'row !important',
		alignItems: 'center',
		justifyContent: 'space-between',
		position: 'fixed',
		zIndex: '1500 !important',
	},
	div: {
		display: 'flex',
		alignItems: 'center',
	},
	img: {
		width: '5em',
	},
	headerItems: {
		fontFamily: 'Lato !important',
	},
	headerIcons: {
		fontSize: 32,
	},
	responsiveMenu: {
		'& .MuiPaper-root': {
			color: '#fff',
			backgroundColor: 'rgba(0,0,0,.9)',
			marginTop: '1em',
			padding: '0 2em',
			borderTop: '2px solid #fff',
			borderRadius: 0,
			overflow: 'visible',
			transition:
				'opacity 296ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, transform 197ms cubic-bezier(0, 0, 0.2, 1) 0ms !important',
			transitionDuration: '0.8s !important',
		},
		'&:after': {
			content: '""',
			display: 'block',
			position: 'absolute',
			top: 81,
			left: arrowPosition => arrowPosition,
			width: 12,
			height: 12,
			background:
				'linear-gradient(137deg, rgba(255,255,255,1) 50%, rgba(0,0,0,0) 50%)',
			transform: 'translateY(-60%) rotate(45deg)',
			zIndex: -2,
		},
	},
	responsiveMenuItems: {
		'& .MuiList-root': {
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
		},
		'& .MuiMenuItem-root': {
			margin: '0.5em 0',
		},
		'& .css-ahj2mt-MuiTypography-root': {
			fontSize: 12,
		},
	},
	searchInput: {
		backgroundColor: 'rgba(0,0,0,.75)',
		'& .MuiSvgIcon-root': {
			color: '#fff',
		},
		'& .Mui-focused fieldset': {
			borderWidth: '0px !important',
		},
		'& .MuiOutlinedInput-root': {
			color: '#fff',
			height: '2em',
			border: '1px solid rgba(255,255,255,.85)',
		},
	},
	notificationsMenu: {
		width: '70%',
		cursor: 'pointer',
		lineHeight: 0.4,
		'& > h3': {
			color: '#CCCCCC',
			fontSize: 14,
			textTransform: 'capitalize',
		},
		'& > p': {
			color: '#808080',
			fontSize: 12,
		},
	},
	movieImg: {
		width: 132,
		height: 64,
		borderRadius: 4,
		cursor: 'pointer',
		objectFit: 'cover',
		marginRight: 12,
	},
}));

// Functions
const getDifferenceAtDays = release_date => {
	const releaseDate = new Date(release_date);
	const differenceDate = Math.abs(new Date() - releaseDate);
	const differenceAtDays = Math.floor(differenceDate / (1000 * 3600 * 24));

	return differenceAtDays;
};

//Render
const Header = () => {
	const [menuArrowPosition, setMenuArrowPosition] = useState(0);
	const classes = useStyles(menuArrowPosition);
	const menuList = ['Home', 'Series', 'Movies', 'Popular news', 'My list'];
	const [anchorMenu, setAnchorMenu] = useState(null);
	const [anchorNotifications, setAnchorNotifications] = useState(null);
	const [anchorMenuProfile, setAnchorMenuProfile] = useState(null);
	const [showSearchView, setShowSearchView] = useState(false);
	const [searchValue, setSearchValue] = useState('');
	const menuProfile = ['Account', 'Help', 'Sing out'];
	const dataApi = MoviesApi('movie', 'upcoming');

	return (
		<AppBar className={classes.root}>
			<div className={classes.div}>
				<IconButton onClick={() => setShowSearchView(false)}>
					<img
						className={classes.img}
						src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png"
						alt=""
					></img>
				</IconButton>
				<Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
					<IconButton
						aria-controls="menu-appbar"
						aria-haspopup="true"
						onClick={({ currentTarget }) => {
							!Boolean(anchorMenu)
								? setAnchorMenu(currentTarget)
								: setAnchorMenu(null);
							setAnchorNotifications(null);
							setAnchorMenuProfile(null);
							setMenuArrowPosition(
								currentTarget.offsetLeft + currentTarget.offsetWidth / 2 - 6
							);
						}}
						color="inherit"
					>
						<Typography>Explore</Typography>
						<ArrowDropDownIcon />
					</IconButton>
					<Menu
						className={`${classes.responsiveMenuItems} ${classes.responsiveMenu}`}
						id="menu-appbar"
						anchorEl={anchorMenu}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'center',
						}}
						transformOrigin={{
							vertical: 'top',
							horizontal: 'center',
						}}
						open={Boolean(anchorMenu)}
						onClose={() => setAnchorMenu(null)}
						sx={{
							display: { xs: 'block', md: 'none' },
						}}
					>
						{menuList.map(menuItem => (
							<MenuItem key={menuItem} onClick={() => setAnchorMenu(null)}>
								<Typography>{menuItem}</Typography>
							</MenuItem>
						))}
					</Menu>
				</Box>
				<Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
					{menuList.map(menuItem => (
						<MenuItem
							className={classes.headerItems}
							key={menuItem}
							onClick={() => setAnchorMenu(null)}
							sx={{ my: 2, color: 'white', display: 'block' }}
						>
							{menuItem}
						</MenuItem>
					))}
				</Box>
			</div>
			<div className={classes.div}>
				<Box sx={{ display: 'flex', alignItems: 'center' }}>
					{showSearchView ? (
						<TextField
							className={classes.searchInput}
							variant="outlined"
							placeholder="Search"
							value={searchValue}
							InputProps={{
								id: 'searchInput',
								startAdornment: (
									<InputAdornment position="start">
										<SearchIcon />
									</InputAdornment>
								),
								endAdornment: (
									<InputAdornment position="end">
										<IconButton onClick={() => setShowSearchView(false)}>
											<CloseIcon />
										</IconButton>
									</InputAdornment>
								),
								autoComplete: 'off',
							}}
							autoFocus
							onChange={({ currentTarget }) => {
								setSearchValue(currentTarget.value);
							}}
						/>
					) : (
						<IconButton
							className={classes.headerIcons}
							id="searchButton"
							size="large"
							aria-label="search"
							color="inherit"
							onClick={() => setShowSearchView(true)}
						>
							<SearchIcon />
						</IconButton>
					)}
					<IconButton
						className={classes.headerIcons}
						size="large"
						aria-label="show new notifications"
						aria-controls="notifications"
						onClick={({ currentTarget }) => {
							!Boolean(anchorNotifications)
								? setAnchorNotifications(currentTarget)
								: setAnchorNotifications(null);
							setAnchorMenu(null);
							setAnchorMenuProfile(null);

							setMenuArrowPosition(
								currentTarget.offsetLeft + currentTarget.offsetWidth / 2 - 6
							);
						}}
						color="inherit"
					>
						<Badge badgeContent={4} color="error">
							<NotificationsIcon />
						</Badge>
					</IconButton>
					<Menu
						className={`${classes.responsiveMenuItems} ${classes.responsiveMenu}`}
						id="notifications"
						anchorEl={anchorNotifications}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'center',
						}}
						transformOrigin={{
							vertical: 'top',
							horizontal: 'center',
						}}
						open={Boolean(anchorNotifications)}
						onClose={() => setAnchorNotifications(null)}
					>
						<div>
							{dataApi.slice(0, 4).map((movie, index) => {
								return (
									<div className={classes.div} key={index}>
										<img
											className={classes.movieImg}
											src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
											alt=""
										></img>
										<div className={classes.notificationsMenu}>
											<h3>Premiere</h3>
											<h3>{movie.title}</h3>
											<p>
												{getDifferenceAtDays(movie.release_date) < 1
													? 'Today'
													: `${getDifferenceAtDays(
															movie.release_date
													  )} days ago`}
											</p>
										</div>
										<Divider />
									</div>
								);
							})}
						</div>
					</Menu>
					<div className={`${classes.headerIcons} ${classes.div}`}>
						<IconButton
							sx={{ paddingLeft: 2, color: 'inherit' }}
							aria-controls="menu-profile"
							onClick={({ currentTarget }) => {
								!Boolean(anchorMenuProfile)
									? setAnchorMenuProfile(currentTarget)
									: setAnchorMenuProfile(null);
								setAnchorMenu(null);
								setAnchorNotifications(null);

								setMenuArrowPosition(
									currentTarget.offsetLeft + currentTarget.offsetWidth / 2 - 14
								);
							}}
						>
							<Avatar
								sx={{
									bgcolor: 'red',
									width: 32,
									height: 34,
									position: 'relative',
								}}
								variant="rounded"
							>
								<AvatarIcon
									sx={{
										fontSize: 26,
										position: 'absolute',
										bottom: 0,
										right: 0,
									}}
								/>
							</Avatar>
							{anchorMenuProfile ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
						</IconButton>
					</div>
					<Menu
						className={`${classes.responsiveMenuItems} ${classes.responsiveMenu}`}
						id="menu-profile"
						anchorEl={anchorMenuProfile}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'center',
						}}
						transformOrigin={{
							vertical: 'top',
							horizontal: 'center',
						}}
						open={Boolean(anchorMenuProfile)}
						onClose={() => setAnchorMenuProfile(null)}
					>
						{menuProfile.map(menuItem => (
							<MenuItem
								key={menuItem}
								onClick={() => setAnchorMenuProfile(null)}
							>
								<Typography>{menuItem}</Typography>
								<Divider />
							</MenuItem>
						))}
					</Menu>
				</Box>
			</div>
			{showSearchView && (
				<SearchView open={showSearchView} searchValue={searchValue} />
			)}
		</AppBar>
	);
};

export default Header;
