import React, { useState } from 'react';
import { MoviesApi } from '../helpers/MoviesApi';
import PreviewModal from './PreviewModal';

import {
	Card,
	CardMedia,
	CardActionArea,
	Stack,
	IconButton,
} from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
	root: {
		height: 800,
		position: 'relative',
		bottom: 0,
	},
	listsContainer: {
		width: '100%',
		height: 840,
		overflow: 'hidden',
		paddingLeft: 24,
		position: 'absolute',
		top: '-48px',
		zIndex: 3,
		'& > div > p': {
			position: 'absolute',
			top: 0,
			color: '#fff',
			fontSize: 16,
			fontWeight: 600,
		},
	},
	cardListContainer: {
		height: 'max-content',
		position: 'relative !important',
		marginBottom: 56,
	},
	cardList: {
		height: 200,
		position: 'absolute',
		top: 48,
		overflowX: 'scroll',
		width: '100%',
		paddingBottom: '12px',
		scrollBehavior: 'smooth',
		'&::-webkit-scrollbar': {
			display: 'none',
		},
	},
	fistCard: {
		'&:hover': {
			transform: 'scale(1.2)',
			transformOrigin: '0% 50%',
			transition: '0.5s',
			zIndex: 1,
		},
	},
	lastCard: {
		'&:hover': {
			transform: 'scale(1.2)',
			transformOrigin: '100% 50%',
			transition: '0.5s',
			zIndex: 1,
		},
	},
	card: {
		'&:hover': {
			transform: 'scale(1.2)',
			transition: '0.5s',
			zIndex: 1,
		},
	},
	cardImg: {
		backgroundColor: 'rgba(10,10,10,.6)',
		width: '236px !important',
	},
	cardListScrollButton: {
		color: '#fff !important',
		opacity: '0.5 !important',
		position: 'sticky !important',
		marginTop: '96px !important',
		height: '100%',
		zIndex: 4,
		'&:hover': {
			transform: 'scale(1.4)',
			transition: '0.4s',
			opacity: '1 !important',
		},
	},
	scrollButtonRight: {
		left: '95%',
	},
	scrollButtonLeft: {
		left: 0,
	},
}));

const CategoryCardLists = () => {
	const classes = useStyles();
	const [movieData, setMovieData] = useState(null);
	const [movieType, setMovieType] = useState(null);
	const [showModal, setShowModal] = useState(false);

	const categoryLists = [
		{
			categoryName: 'Popular on Netflix',
			movie: MoviesApi('movie', 'upcoming'),
			movieType: 'movie',
		},
		{
			categoryName: 'Continue Watching',
			movie: MoviesApi('movie', 'top_rated'),
			movieType: 'movie',
		},
		{
			categoryName: 'Trends',
			movie: MoviesApi('movie', 'popular'),
			movieType: 'movie',
		},
		{
			categoryName: 'Series',
			movie: MoviesApi('tv', 'popular'),
			movieType: 'tv',
		},
	];

	const handleScrollRight = e => {
		const element = e.currentTarget.nextElementSibling.nextElementSibling;
		element.offsetWidth + element.scrollLeft >= element.scrollWidth
			? (element.scrollLeft = 0)
			: (element.scrollLeft += element.offsetWidth);
	};
	const handleScrollLeft = e => {
		const element =
			e.currentTarget.nextElementSibling.nextElementSibling.nextElementSibling;
		element.scrollLeft === 0
			? (element.scrollLeft = element.scrollWidth - element.offsetWidth)
			: (element.scrollLeft -= element.offsetWidth);
	};

	// Render
	return (
		<div className={classes.root}>
			<div className={classes.listsContainer}>
				{categoryLists.map((category, index) => {
					return (
						<div
							className={classes.cardListContainer}
							key={`cardList-${category.categoryName}-${index}`}
						>
							<IconButton
								onClick={handleScrollLeft}
								className={`${classes.cardListScrollButton} ${classes.scrollButtonLeft}`}
							>
								<ArrowBackIosIcon sx={{ fontSize: 32 }} />
							</IconButton>
							<IconButton
								onClick={handleScrollRight}
								className={`${classes.cardListScrollButton} ${classes.scrollButtonRight}`}
							>
								<ArrowForwardIosIcon sx={{ fontSize: 32 }} />
							</IconButton>
							<p>{category.categoryName}</p>
							<div className={classes.cardList}>
								<Stack
									direction="row"
									spacing={1}
									style={{ width: 'max-content', paddingTop: 14 }}
								>
									{category.movie.map((movie, index) => {
										return (
											<Card
												key={`movie-${category.categoryName}-${index}`}
												sx={{ width: 236 }}
												className={
													index === 0
														? classes.fistCard
														: index + 1 === category.movie.length
														? classes.lastCard
														: classes.card
												}
											>
												<CardActionArea>
													<CardMedia
														data-movie-type={category.movieType}
														className={classes.cardImg}
														component="img"
														height="136"
														image={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
														onClick={({ currentTarget }) => {
															setMovieType(currentTarget.dataset.movieType);
															setMovieData(movie);
															setShowModal(true);
														}}
														alt="movieImg"
													/>
												</CardActionArea>
											</Card>
										);
									})}
								</Stack>
							</div>
						</div>
					);
				})}
				<PreviewModal
					movieType={movieType}
					movieData={movieData}
					open={showModal}
					setOpen={setShowModal}
				/>
			</div>
		</div>
	);
};

export default CategoryCardLists;
