import React, { useState, useEffect } from 'react';
import { MoviesApi, VideosApi } from '../helpers/MoviesApi';
import PreviewModal from './PreviewModal';
import { Link } from 'react-router-dom';
import { Container, Button, Stack, IconButton } from '@mui/material';
import YouTube from 'react-youtube';

// Icons
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import VolumeUpOutlinedIcon from '@mui/icons-material/VolumeUpOutlined';
import VolumeOffOutlinedIcon from '@mui/icons-material/VolumeOffOutlined';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	container: {
		backgroundColor: '#141414',
		height: 616,
		padding: '0px !important',
		position: 'relative',
	},
	imgPreview: {
		position: 'absolute',
		top: 0,
		width: '100%',
		height: '100%',
		objectFit: 'cover',
		zIndex: 0,
	},
	iframePreview: {
		position: 'absolute',
		top: 0,
		width: '100%',
		height: '100%',
		border: 'none',
		zIndex: 0,
	},
	movieDetailsContainer: {
		background:
			'linear-gradient(0deg, rgba(20,20,20,1) 0%, rgba(255,255,255,0) 40%)',
		height: '100%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		zIndex: 3,
		position: 'absolute',
	},
	movieDetails: {
		color: '#fff',
		width: '50%',
		fontWeight: '600',
		paddingLeft: '24px',
		'& > p': {
			fontSize: '0.9em',
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			display: '-webkit-box',
			'-webkit-box-orient': 'vertical',
			'-webkit-line-clamp': 4,
		},
	},
	buttons: {
		textTransform: 'none !important',
		fontWeight: '600 !important',
	},
	primaryButton: {
		backgroundColor: '#fff !important',
		color: '#141414 !important',
	},
	secondaryButton: {
		backgroundColor: 'rgba(109, 109, 110, 0.7) !important',
		color: '#fff !important',
		border: 'none !important',
		'&:focus': {
			outline: '2px solid #fff',
		},
	},
	volumeControl: {
		display: 'flex',
		alignItems: 'center',
	},
	minAge: {
		color: '#fff',
		backgroundColor: 'rgba(51,51,51,.6)',
		width: 'max-content',
		height: 'max-content',
		padding: '4px 20px',
		marginLeft: 8,
		borderLeft: '2px solid #fff',
		display: 'flex',
		alignItems: 'center',
		'& > p': {
			marginBlockStart: 0,
			marginBlockEnd: 0,
		},
	},
});

// Render
const Hero = () => {
	const classes = useStyles();
	const dataApi = MoviesApi('movie', 'upcoming');

	const [movieSrc, setMovieSrc] = useState([]);
	const [showVideo, setShowVideo] = useState(true);
	const [mutedVideo, setMutedVideo] = useState(1);
	const [openPreviewModal, setOpenPreviewModal] = useState(false);

	useEffect(() => {
		if (dataApi && dataApi.length > 0) {
			VideosApi('movie', dataApi[0].id, setMovieSrc);
			localStorage.setItem('type', 'movie');
			localStorage.setItem('id', dataApi[0].id);
		}
	}, [dataApi, dataApi.length]);

	return (
		<Container maxWidth="xl" className={classes.container}>
			{dataApi && dataApi.length > 0 ? (
				<>
					{movieSrc.length > 0 && showVideo ? (
						<YouTube
							className={classes.iframePreview}
							videoId={movieSrc[0].key}
							opts={{
								height: '100%',
								width: '100%',
								playerVars: {
									autoplay: 1,
									controls: 0,
									mute: mutedVideo,
									disablekb: 1,
									fs: 0,
									iv_load_policy: 3,
									rel: 0,
									showinfo: 0,
								},
							}}
							onEnd={() => {
								setShowVideo(false);
							}}
						/>
					) : (
						<img
							className={classes.imgPreview}
							src={`https://image.tmdb.org/t/p/w500${dataApi[0].poster_path}`}
							alt=""
						/>
					)}
					<div className={classes.movieDetailsContainer}>
						<div className={classes.movieDetails}>
							<h2>{dataApi[0].title}</h2>
							<p>{dataApi[0].overview}</p>
							<Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
								<Link to="/movie" style={{ textDecoration: 'none' }}>
									<Button
										className={`${classes.buttons} ${classes.primaryButton}`}
										variant="contained"
										startIcon={<PlayArrowIcon />}
									>
										Play
									</Button>
								</Link>
								<Button
									className={`${classes.buttons} ${classes.secondaryButton}`}
									variant="outlined"
									startIcon={<InfoOutlinedIcon />}
									onClick={() => setOpenPreviewModal(true)}
								>
									More Information
								</Button>
							</Stack>
						</div>
						<div className={classes.volumeControl}>
							{movieSrc.length > 0 && showVideo && (
								<IconButton
									aria-label="volume"
									onClick={() => setMutedVideo(mutedVideo === 0 ? 1 : 0)}
								>
									{Boolean(mutedVideo) ? (
										<VolumeOffOutlinedIcon
											sx={{ color: '#fff', fontSize: '32px' }}
										/>
									) : (
										<VolumeUpOutlinedIcon
											sx={{ color: '#fff', fontSize: '32px' }}
										/>
									)}
								</IconButton>
							)}
							<div className={classes.minAge}>
								<p>+12</p>
							</div>
						</div>
					</div>
				</>
			) : (
				<img className={classes.imgPreview} src="" alt="" />
			)}
			<PreviewModal
				movieType={'movie'}
				movieData={dataApi[0]}
				open={openPreviewModal}
				setOpen={setOpenPreviewModal}
			/>
		</Container>
	);
};

export default Hero;
