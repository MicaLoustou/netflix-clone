import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { VideosApi } from '../helpers/MoviesApi';
import { IconButton } from '@mui/material';
import YouTube from 'react-youtube';

import CloseIcon from '@mui/icons-material/Close';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
	closeButton: {
		backgroundColor: 'rgba(51,51,51,.6) !important',
		color: '#fff !important',
		position: 'absolute',
		top: 12,
		left: '95%',
		zIndex: 2,
	},
	iframePreview: {
		position: 'absolute',
		top: 0,
		height: '100vh !important',
	},
}));

const MoviePreview = () => {
	const classes = useStyles();
	const [movieSrc, setMovieSrc] = useState([]);
	const movieType = localStorage.getItem('type');
	const movieId = localStorage.getItem('id');

	useEffect(() => {
		VideosApi(movieType, movieId, setMovieSrc);
	}, [movieType, movieId]);

	return (
		<>
			<Link to="/">
				<IconButton className={classes.closeButton}>
					<CloseIcon />
				</IconButton>
			</Link>
			<YouTube
				className={classes.iframePreview}
				videoId={movieSrc && movieSrc.length > 0 && movieSrc[0].key}
				opts={{
					height: '100%',
					width: '100%',
					playerVars: {
						autoplay: 1,
						disablekb: 1,
						fs: 0,
						iv_load_policy: 3,
						rel: 0,
						showinfo: 0,
					},
				}}
			/>
		</>
	);
};

export default MoviePreview;
