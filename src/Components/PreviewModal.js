import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { DetailsApi } from '../helpers/MoviesApi';
import SimilarMovies from './SimilarMovies';
import { Modal, Box, CardMedia, Button, IconButton, Grid } from '@mui/material';

// Icons
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import AddIcon from '@mui/icons-material/Add';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import ThumbDownAltIcon from '@mui/icons-material/ThumbDownAlt';
import CloseIcon from '@mui/icons-material/Close';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'center',
		overflowY: 'scroll',
		alignItems: 'center',
		zIndex: '1500 !important',
	},
	container: {
		backgroundColor: '#141414',
		position: 'absolute',
		top: 24,
		left: '10%',
		display: 'flex',
		flexDirection: 'column',
		width: '80vw',
		maxWidth: '1200px',
		height: 'max-content',
		minHeight: '80vh',
		borderRadius: 6,
	},
	modalGradient: {
		background:
			'linear-gradient(0deg, rgba(20,20,20,1) 0%, rgba(255,255,255,0) 40%)',
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100%',
		height: 480,
		zIndex: 5,
	},
	img: {
		borderTopLeftRadius: 6,
		borderTopRightRadius: 6,
	},
	titleContainer: {
		color: '#fff',
		position: 'absolute',
		bottom: 74,
		left: 24,
	},
	primaryButton: {
		backgroundColor: '#fff !important',
		color: '#141414 !important',
		textTransform: 'none !important',
		fontWeight: '600 !important',
		marginRight: '12px !important',
	},
	iconButtons: {
		backgroundColor: 'rgba(51,51,51,.6) !important',
		color: '#fff !important',
		marginRight: '12px !important',
	},
	closeButtonContainer: {
		position: 'relative',
	},
	closeButton: {
		top: 12,
		left: '93%',
	},
	detailsContainer: {
		display: 'flex',
		padding: '0 48px',
	},
	overview: {
		color: '#fff',
		fontWeight: 600,
		width: '100%',
	},
	movieDetails: {
		width: 260,
		display: 'flex',
		justifyContent: 'space-between',
	},
	matchPorcent: {
		color: '#44CC66',
	},
	minAge: {
		border: '1px solid #fff',
		padding: '0 16px',
		height: 'max-content',
	},
	gendersContainer: {
		color: '#fff',
		fontSize: 14,
		width: 'max-content',
		marginLeft: '32px',
		overflowWrap: 'break-word',
		'& > p > span': {
			color: '#474747',
		},
	},
	cardContainer: {
		padding: '0 48px',
	},
	similarTitle: {
		color: '#fff',
		fontWeight: 600,
	},
	grid: {
		margin: '0 !important ',
		paddingBottom: 48,
	},
}));

const PreviewModal = ({ movieType, movieData, open, setOpen }) => {
	const classes = useStyles();
	const [gender, setGender] = useState([]);

	useEffect(() => {
		if (movieData && movieType) {
			movieData && movieType && DetailsApi(movieType, movieData?.id, setGender);
			localStorage.setItem('type', movieType);
			localStorage.setItem('id', movieData.id);
		}
	}, [movieData, movieType]);

	return (
		<>
			<Modal
				className={classes.root}
				open={Boolean(movieData?.id && open)}
				onClose={() => setOpen(false)}
			>
				<Box className={classes.container}>
					<div className={classes.modalGradient}>
						<div className={classes.closeButtonContainer}>
							<IconButton
								className={`${classes.iconButtons} ${classes.closeButton}`}
								onClick={() => setOpen(false)}
							>
								<CloseIcon />
							</IconButton>
						</div>
						<div className={classes.titleContainer}>
							<h2>{movieData?.title}</h2>
							<div>
								<Link to="/movie" style={{ textDecoration: 'none' }}>
									<Button
										className={classes.primaryButton}
										variant="contained"
										startIcon={<PlayArrowIcon />}
									>
										Play
									</Button>
								</Link>
								<IconButton
									className={classes.iconButtons}
									variant="outlined"
									aria-label="delete"
								>
									<AddIcon />
								</IconButton>

								<IconButton className={classes.iconButtons} aria-label="delete">
									<ThumbUpAltIcon />
								</IconButton>

								<IconButton className={classes.iconButtons} aria-label="delete">
									<ThumbDownAltIcon />
								</IconButton>
							</div>
						</div>
					</div>
					<CardMedia
						className={classes.img}
						component="img"
						height="480"
						image={`https://image.tmdb.org/t/p/w500${movieData?.poster_path}`}
						alt="Preview Image"
					/>
					<div className={classes.detailsContainer}>
						<div className={classes.overview}>
							<div className={classes.movieDetails}>
								<p className={classes.matchPorcent}>{`${Math.floor(
									Math.random() * (100 - 30) + 30
								)}% for you`}</p>
								<p>
									{movieData?.release_date
										? new Date(movieData?.release_date).getFullYear()
										: ''}
								</p>
								<p className={classes.minAge}>12</p>
								<p>{`${Math.floor(Math.random() * (2 - 1) + 1)} h`}</p>
							</div>
							<p>{movieData?.overview}</p>
						</div>
						<div className={classes.gendersContainer}>
							<p>
								<span>Genders:</span>
								{gender?.genres?.map((el, index) => {
									if (gender.genres.length - 1 === index) {
										return el.name;
									} else return ` ${el.name}, `;
								})}
							</p>
						</div>
					</div>
					<div className={classes.cardContainer}>
						<h2 className={classes.similarTitle}>More similar titles</h2>
						<Grid
							container
							className={classes.grid}
							rowSpacing={2}
							justifyContent="space-evenly"
						>
							<SimilarMovies movieData={movieData} movieType={movieType} />
						</Grid>
					</div>
				</Box>
			</Modal>
		</>
	);
};

export default PreviewModal;
