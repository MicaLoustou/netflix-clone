import React from 'react';
import { Grid, IconButton, Tooltip, Typography } from '@mui/material';

// Icons
import BitbucketIcon from '../assets/BitbucketIcon';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	footerContainer: {
		backgroundColor: '#141414',
		color: 'grey',
		display: 'flex',
		flexDirection: 'column',
		padding: '0 4%',
	},
	iconsContainer: {
		display: 'flex',
		alignItems: 'center',
	},
	bitbucketIcon: {
		paddingTop: 8,
		paddingLeft: 8,
	},
	gridItems: {
		margin: '12px 0',
	},
});

const Footer = () => {
	const classes = useStyles();
	return (
		<div className={classes.footerContainer}>
			<div className={classes.iconsContainer}>
				<Tooltip title="Portfolio">
					<a href="/#">
						<IconButton>
							<AssignmentIndIcon sx={{ fontSize: 30, color: 'white' }} />
						</IconButton>
					</a>
				</Tooltip>
				<Tooltip title="Bitbucket">
					<a
						href="https://bitbucket.org/MicaLoustou/"
						target="_blank"
						rel="noreferrer"
					>
						<IconButton>
							<BitbucketIcon
								className={classes.bitbucketIcon}
								sx={{ fontSize: 36 }}
							/>
						</IconButton>
					</a>
				</Tooltip>
				<Tooltip title="LinkedIn">
					<a
						href="https://www.linkedin.com/in/micaelaloustou/"
						target="_blank"
						rel="noreferrer"
					>
						<IconButton>
							<LinkedInIcon sx={{ fontSize: 30, color: 'white' }} />
						</IconButton>
					</a>
				</Tooltip>
			</div>
			<Grid container className={classes.gridItems}>
				<Grid item xs={3}>
					<Typography variant="body2">Audio and subtitles</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Privacity</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Contact us</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Descriptive audio</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Inversor relations</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Legal notices</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Help center</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Job</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Cookies preferences</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Gift cards</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Terms of use</Typography>
				</Grid>
				<Grid item xs={3}>
					<Typography variant="body2">Corporate information</Typography>
				</Grid>
			</Grid>
			<Typography
				variant="subtitle2"
				component="p"
			>{`MIT license, Micaela Loustou ${new Date().getFullYear()}. (It is not the original Netflix page)`}</Typography>
		</div>
	);
};

export default Footer;
