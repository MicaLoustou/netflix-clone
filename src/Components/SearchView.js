import React, { useState, useEffect } from 'react';
import PreviewModal from './PreviewModal';
import Footer from './Footer';
import { SearchMoviesApi } from '../helpers/MoviesApi';
import { Card, CardMedia, Grid, Modal } from '@mui/material';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
	modal: {
		zIndex: '5 !important',
		overflowY: 'auto',
		'& :first-child': {
			backgroundColor: '#141414',
		},
	},
	resultsContainer: {
		position: 'relative',
		height: '100%',
		width: '100%',
	},
	results: {
		width: '100%',
		position: 'absolute',
		marginTop: 124,
		padding: 16,
		display: 'flex',
		justifyContent: 'center',
	},
	closeButton: {
		backgroundColor: 'rgba(51,51,51,.6) !important',
		color: '#fff !important',
		left: '94%',
	},
	card: {
		cursor: 'pointer',
		'&:hover': {
			transform: 'scale(1.2)',
			transition: '0.5s',
			zIndex: 1,
		},
	},
	img: {
		width: '236px',
	},
	notFound: {
		color: '#fff',
		fontWeight: 600,
		width: '100%',
		textAlign: 'center',
	},
	'@media only screen and (min-width: 0px)': {
		gridContainer: {
			width: '228px !important',
			justifyContent: ' center',
		},
	},
	'@media only screen and (min-width: 425px)': {
		gridContainer: {
			width: '468px !important',
		},
	},
	'@media only screen and (min-width: 768px)': {
		gridContainer: {
			width: '708px !important',
			justifyContent: 'flex-start',
		},
	},
	'@media only screen and (min-width: 1024px)': {
		gridContainer: {
			width: '948px !important',
		},
	},
	'@media only screen and (min-width: 1440px)': {
		gridContainer: {
			width: '1428px !important',
		},
	},
	'@media only screen and (min-width: 2560px)': {
		gridContainer: {
			width: '2388px !important',
		},
	},
}));

// Functions
const calcOffsetHeight = setOffsetHeight => {
	const gridContainer = document.querySelector('#gridContainer');
	const viewHeight = window.innerHeight;

	if (gridContainer) {
		if (gridContainer.offsetHeight + 300 > viewHeight) {
			setOffsetHeight(gridContainer.offsetHeight + 120);
		} else {
			setOffsetHeight('80vh');
		}
	} else {
		setOffsetHeight('80vh');
	}
};

const SearchView = ({ open, searchValue }) => {
	const classes = useStyles();
	const [searchResults, setSearchResults] = useState([]);
	const [openPreviewModal, setOpenPreviewModal] = useState(false);
	const [movieData, setMovieData] = useState({});
	const [offsetHeight, setOffsetHeight] = useState('80vh');

	useEffect(() => {
		if (searchValue && searchValue !== '') {
			SearchMoviesApi(searchValue, setSearchResults);
		}

		window.addEventListener('resize', () => calcOffsetHeight(setOffsetHeight));

		if (searchResults && searchResults.length > 0) {
			calcOffsetHeight(setOffsetHeight);
		} else {
			setOffsetHeight('80vh');
		}

		return () =>
			window.removeEventListener('resize', () =>
				calcOffsetHeight(setOffsetHeight)
			);
	}, [searchValue, searchResults, searchResults.length]);

	return (
		<Modal open={open} className={classes.modal}>
			<>
				<div style={{ height: offsetHeight }}>
					<div className={classes.results}>
						{searchResults && searchResults.length > 0 ? (
							<Grid
								container
								className={classes.gridContainer}
								id="gridContainer"
								spacing={1}
								rowSpacing={8}
								columns={{ sm: 2, md: 4, lg: 5 }}
							>
								{searchResults.map((el, index) => {
									if (el.media_type !== 'person' && el.poster_path !== null) {
										return (
											<Grid className={classes.img} item key={index}>
												<Card className={classes.card}>
													<CardMedia
														component="img"
														height="136"
														image={`https://image.tmdb.org/t/p/w500${el.poster_path}`}
														alt="movieImg"
														onClick={() => {
															setOpenPreviewModal(true);
															setMovieData(el);
														}}
													/>
												</Card>
											</Grid>
										);
									}
									return null;
								})}
							</Grid>
						) : (
							<p className={classes.notFound}>
								{searchValue && searchValue !== '' ? 'Movie not found' : ''}
							</p>
						)}
					</div>
				</div>
				<PreviewModal
					movieType={movieData.media_type}
					movieData={movieData}
					open={openPreviewModal}
					setOpen={setOpenPreviewModal}
				/>
				<Footer />
			</>
		</Modal>
	);
};

export default SearchView;
