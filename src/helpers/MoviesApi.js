import { useState, useEffect } from 'react';

export const MoviesApi = (type, category) => {
	const [dataApi, setDataApi] = useState([]);

	useEffect(() => {
		fetch(
			`https://api.themoviedb.org/3/${type}/${category}?api_key=bf76db1e6a910459b9c6a8ce6d5da876`
		)
			.then(res => res.json())
			.then(json => setDataApi(json.results));
	}, [type, category]);

	return dataApi;
};

export const VideosApi = (type, id, updateState) => {
	fetch(
		`https://api.themoviedb.org/3/${type}/${id}/videos?api_key=bf76db1e6a910459b9c6a8ce6d5da876`
	)
		.then(res => res.json())
		.then(json => updateState(json.results));
};

export const DetailsApi = (type, id, updateState) => {
	fetch(
		`https://api.themoviedb.org/3/${type}/${id}?api_key=bf76db1e6a910459b9c6a8ce6d5da876`
	)
		.then(res => res.json())
		.then(json => updateState(json));
};

export const SimilarMoviesApi = (type, id, updateState) => {
	fetch(
		`https://api.themoviedb.org/3/${type}/${id}/similar?api_key=bf76db1e6a910459b9c6a8ce6d5da876`
	)
		.then(res => res.json())
		.then(json => updateState(json));
};

export const SearchMoviesApi = (searchValue, updateState) => {
	fetch(
		`https://api.themoviedb.org/3/search/multi?api_key=bf76db1e6a910459b9c6a8ce6d5da876&language=en-US&query=${searchValue}&page=1&include_adult=false`
	)
		.then(res => res.json())
		.then(json => updateState(json.results));
};
