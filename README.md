# Netflix Clone 🎥

---

I made a dynamic and responsive copy of the Netflix home page and search engine with React.js, inspecting the official Netflix page.

## Watch the project on Netlify 👀

---

### [Netlify: Netflix Clone](https://inspiring-lamport-4306c3.netlify.app/).

## What does the project look like? 🎨

---

1. Index.
   ![index](./src/assets/readme/index.png)
   ![search](./src/assets/readme/search.png)

## Tools 🔧

---

For build this project i used:

- [React.js](https://es.reactjs.org/)
- [Material UI](https://mui.com/)
- [react-youtube](https://www.npmjs.com/package/react-youtube)
- APIs REST like - [The Movie Database](https://www.themoviedb.org/)

## To run this project 🏃‍♂️

---

1. Clone this repository.
2. Open the terminal located in the project.
3. Run the following command `npm install`.
4. Run the following command `npm start`.

---

⌨️ with ❤️ by [Mica Loustou](https://bitbucket.org/MicaLoustou/) 😊
